// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"

// Sets default values
APickup::APickup()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>("PickupMesh");
    PickupMesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.3f));
    FCollisionResponseContainer* ResponseContainer = new FCollisionResponseContainer();
    ResponseContainer->SetAllChannels(ECR_Ignore);
    ResponseContainer->SetResponse(ECC_Visibility, ECR_Block);
    PickupMesh->BodyInstance.SetResponseToChannels(*ResponseContainer);
    PickupMesh->BodyInstance.SetObjectType(ECC_WorldDynamic);

    RootComponent = PickupMesh;

    PickupBox = CreateDefaultSubobject<UBoxComponent>("PickupBox");
    PickupBox->SetWorldScale3D(FVector(15.f, 15.f, 8.f));
    PickupBox->SetRelativeLocation(FVector(0.f, 0.f, 215.f));
    PickupBox->bGenerateOverlapEvents = true;
    PickupBox->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnPlayerEnterPickupBox);
    PickupBox->OnComponentEndOverlap.AddDynamic(this, &APickup::OnPlayerExitPickupBox);
    ResponseContainer->SetResponse(ECC_Pawn, ECR_Overlap);
    ResponseContainer->SetResponse(ECC_Visibility, ECR_Ignore);
    PickupBox->BodyInstance.SetResponseToChannels(*ResponseContainer);
    PickupBox->BodyInstance.SetObjectType(ECC_WorldDynamic);
    PickupBox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void APickup::OnPlayerEnterPickupBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (AProjectCharacter* pCharacter = Cast<AProjectCharacter>(OtherActor))
    {
        pCharacter->bCanInteraction++;
        PickupMesh->SetRenderCustomDepth(true);
    }
}

void APickup::OnPlayerExitPickupBox(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
    if (AProjectCharacter* pCharacter = Cast<AProjectCharacter>(OtherActor))
    {
        pCharacter->bCanInteraction--;
        PickupMesh->SetRenderCustomDepth(false);
        PickupMesh->SetVisibility(true);
    }
}

